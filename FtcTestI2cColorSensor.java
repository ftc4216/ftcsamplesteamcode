/*
 * Copyright (c) 2015 Titan Robotics Club (http://www.titanrobotics.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.firstinspires.ftc.teamcode.trc492;

import android.widget.TextView;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.ColorSensor;

import TrcFtcLib.ftclib.FtcMRI2cColorSensor;
import TrcFtcLib.ftclib.FtcOpMode;
import TrcCommonLib.trclib.TrcDashboard;
import TrcCommonLib.trclib.TrcRobot;
import TrcCommonLib.trclib.TrcSensor;

@TeleOp(name="Test: I2C Color Sensor", group="FtcTestSamples")
@Disabled
public class FtcTestI2cColorSensor extends FtcOpMode
{
    private static final int ALTERNATE_I2CADDRESS = 0x40;

    private TrcDashboard dashboard;
    private FtcMRI2cColorSensor i2cColorSensor;
    private ColorSensor colorSensor;

    //
    // Implements FtcOpMode abstract methods.
    //

    @Override
    public void initRobot()
    {
        hardwareMap.logDevices();
        dashboard = TrcDashboard.getInstance();

//        i2cColorSensor = new FtcMRI2cColorSensor("i2cColorSensor");
        i2cColorSensor = new FtcMRI2cColorSensor("i2cColorSensor", ALTERNATE_I2CADDRESS, false);
//        i2cColorSensor.setLEDEnabled(false);
        colorSensor = hardwareMap.colorSensor.get("colorSensor");
    }   //initRobot

    //
    // Overrides TrcRobot.RobotMode methods.
    //

    @Override
    public void startMode(TrcRobot.RunMode prevMode, TrcRobot.RunMode nextMode)
    {
    }   //startMode

    @Override
    public void runPeriodic(double elapsedTime)
    {
        final int LABEL_WIDTH = 200;
        dashboard.displayPrintf(1, "FirmwareRev: %x", i2cColorSensor.getFirmwareRevision());
        dashboard.displayPrintf(2, "ManufacturerCode: %x", i2cColorSensor.getManufacturerCode());
        dashboard.displayPrintf(3,"IDCode: %x", i2cColorSensor.getIdCode());
        TrcSensor.SensorData data = i2cColorSensor.getColorNumber();
        //
        // The data may not be ready yet, check it!
        //
        if (data.value != null)
        {
            dashboard.displayPrintf(4,  "ColorNumber: %.0f", i2cColorSensor.getColorNumber().value);
            dashboard.displayPrintf(5, "RedValue: %.0f", i2cColorSensor.getRedValue().value);
            dashboard.displayPrintf(6,  "GreenValue: %.0f", i2cColorSensor.getGreenValue().value);
            dashboard.displayPrintf(7, "BlueValue: %.0f", i2cColorSensor.getBlueValue().value);
            dashboard.displayPrintf(8, "WhiteValue: %.0f", i2cColorSensor.getWhiteValue().value);
        }
        dashboard.displayPrintf(9,  "W/R/G/B: %02x%02x%02x%02x (%d/%d/%d/%d)",
                                colorSensor.alpha(), colorSensor.red(), colorSensor.green(), colorSensor.blue(),
                                colorSensor.alpha(), colorSensor.red(), colorSensor.green(), colorSensor.blue());
        dashboard.displayPrintf(10,  "Hue: %08x", colorSensor.argb());
    }   //runPeriodic

}   //class FtcTestI2cColorSensor
