/*
 * Copyright (c) 2016 Titan Robotics Club (http://www.titanrobotics.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.firstinspires.ftc.teamcode.trc492;

import android.widget.TextView;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import TrcFtcLib.ftclib.FtcMRI2cGyro;
import TrcFtcLib.ftclib.FtcOpMode;
import TrcCommonLib.trclib.TrcDashboard;
import TrcCommonLib.trclib.TrcRobot;
import TrcCommonLib.trclib.TrcSensor;

@TeleOp(name="Test: Modern Robotics I2C Gyro", group="FtcTestSamples")
@Disabled
public class FtcTestMRI2cGyro extends FtcOpMode
{
    private TrcDashboard dashboard;
    private FtcMRI2cGyro gyro;

    //
    // Implements FtcOpMode abstract methods.
    //

    @Override
    public void initRobot()

    {
        hardwareMap.logDevices();
        dashboard = TrcDashboard.getInstance();
        //FtcRobotControllerActivity activity = (FtcRobotControllerActivity)hardwareMap.appContext;
        //dashboard.setTextView((TextView)activity.findViewById(R.id.textOpMode));
        gyro = new FtcMRI2cGyro("mrGyro");
        gyro.calibrate();
    }   //initRobot

    //
    // Overrides TrcRobot.RobotMode methods.
    //

    @Override
    public void startMode(TrcRobot.RunMode prevMode, TrcRobot.RunMode nextMode)
    {
        // dashboard.clearDisplay();
    }   //startMode

    @Override
    public void runPeriodic(double elapsedTime)
    {
        final int LABEL_WIDTH = 200;
        dashboard.displayPrintf(1, "FirmwareRev: %x", gyro.getFirmwareRevision());
        dashboard.displayPrintf(2, "IDCode: %x", gyro.getIdCode());
        TrcSensor.SensorData data = gyro.getHeading();
        //
        // The data may not be ready yet, check it!
        //
        if (data.value != null)
        {
            dashboard.displayPrintf(4, "Heading: %.0f", gyro.getHeading().value);
            dashboard.displayPrintf(5,  "IntegratedZ: %.0f", gyro.getIntegratedZ().value);
            dashboard.displayPrintf(6,  "RawXYZ: %.0f/%.0f/%.0f",
                                    gyro.getRawX().value, gyro.getRawY().value, gyro.getRawZ().value);
            dashboard.displayPrintf(7, "ZOffset: %.0f", gyro.getZOffset().value);
            dashboard.displayPrintf(8, "ZScaling: %.0f", gyro.getZScaling().value);
        }
    }   //runPeriodic

}   //class FtcTestMRI2cGyro
