/*
 * Copyright (c) 2016 Titan Robotics Club (http://www.titanrobotics.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.firstinspires.ftc.teamcode.trc492;

import android.widget.TextView;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

//import org.firstinspires.ftc.robotcontroller.internal.FtcRobotControllerActivity;
//
//import FtcSampleCode.R;
import TrcFtcLib.ftclib.FtcOpMode;
import TrcFtcLib.ftclib.FtcZXDistanceSensor;
import TrcCommonLib.trclib.TrcDashboard;
import TrcCommonLib.trclib.TrcRobot;
import TrcCommonLib.trclib.TrcSensor;

@TeleOp(name="Test: ZX Distance Sensor", group="FtcTestSamples")
@Disabled
public class FtcTestZXDistanceSensor extends FtcOpMode
{
    private TrcDashboard dashboard;
    private FtcZXDistanceSensor sensor;

    //
    // Implements FtcOpMode abstract methods.
    //

    @Override
    public void initRobot()
    {
        hardwareMap.logDevices();
        dashboard = TrcDashboard.getInstance();
//        FtcRobotControllerActivity activity = (FtcRobotControllerActivity)hardwareMap.appContext;
//        dashboard.setTextView((TextView)activity.findViewById(R.id.textOpMode));
        sensor = new FtcZXDistanceSensor("zxSensor", FtcZXDistanceSensor.ALTERNATE_I2CADDRESS, false);
    }   //initRobot

    //
    // Overrides TrcRobot.RobotMode methods.
    //

    @Override
    public void startMode(TrcRobot.RunMode prevMode, TrcRobot.RunMode nextMode)
    {
//        dashboard.clearDisplay();
    }   //startMode

    @Override
    public void runPeriodic(double elapsedTime)
    {
        final int LABEL_WIDTH = 200;
        TrcSensor.SensorData data;

        dashboard.displayPrintf(1,  "Model: %d", sensor.getModelVersion());
        dashboard.displayPrintf(2,  "RegVersion: %d", sensor.getRegMapVersion());
        dashboard.displayPrintf(3, "Status: %02x", sensor.getStatus());

        //
        // The data may not be ready yet, check it!
        //

        data = sensor.getGesture();
        if (data.value != null)
        {
            dashboard.displayPrintf(4, "Gesture: %s", data.value.toString());
        }

        data = sensor.getGestureSpeed();
        if (data.value != null)
        {
            dashboard.displayPrintf(5,  "GestureSpeed: %d", data.value);
        }

        data = sensor.getX();
        if (data.value != null)
        {
            dashboard.displayPrintf(6, "X: %d", data.value);
        }

        data = sensor.getZ();
        if (data.value != null)
        {
            dashboard.displayPrintf(7, "Z:%d", data.value);
        }

        data = sensor.getLeftRangingData();
        if (data.value != null)
        {
            dashboard.displayPrintf(8, "LRng: %d", data.value);
        }

        data = sensor.getRightRangingData();
        if (data.value != null)
        {
            dashboard.displayPrintf(9, "RRng: %d", data.value);
        }
    }   //runPeriodic

}   //class FtcTestAndroidSensors
